<?php
class CSVManager {

    private $_csv_file = null;
    private $explSym;

    public function __construct($csv_file, $explSym = ',')
    {
        $this->explSym = $explSym;
        if (file_exists($csv_file)) {
            $this->_csv_file = $csv_file;
        } else {
            throw new Exception("Файл {$csv_file} не найден");
        }
    }

    public function setCSV(Array $csv)
    {
        $handle = fopen($this->_csv_file, "a");
        foreach ($csv as $value) {
            fputcsv($handle, explode($this->explSym, $value), ";");
        }
        fclose($handle);
    }

    public function getCSV()
    {
        $handle = fopen($this->_csv_file, "r");

        $array_line_full = array();
        while (($line = fgetcsv($handle, 0, $this->explSym)) !== FALSE) {
            $array_line_full[] = $line;
        }
        fclose($handle);
        return $array_line_full;
    }

    public function setFileLocation($csv_file, $explSym = ',')
    {
      $this->_csv_file = $csv_file;
      if ($explSym !== ',') {
          $this->explSym = $explSym;
      }
    }
}
?>
