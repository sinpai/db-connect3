<?php
require_once 'DriverInterface.php';

class MongoDBDriver implements DriverInterface
{
    const SORT_ASC = 'ASC';
    const SORT_DESC = 'DESC';
    private $conn;
    private $db_name;
    private $cursor;
    private $result = array();

    public function __construct($conn_obj, $db_name)
    {
        $this->conn = $conn_obj;
        $this->db_name = $db_name;
    }

    public function createTable($name, $options = null)
    {
        try {
            $cmd = new MongoDB\Driver\Command([
                'create' => $name
            ]);
            $this->conn->executeCommand($this->db_name, $cmd);
        } catch (MongoDB\Driver\Exception\RuntimeException $e) {
            $this->delete($name, array(), true);
            return true;
        }
    }

    public function alterTable($name, $options, $field)
    {
        $filter = array();
        $update = ['$set' => $field];
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update($filter, $update, ['multi' => true, 'upsert' => false]);
        try {
            $result = $this->conn->executeBulkWrite("{$this->db_name}.{$name}", $bulk);
        } catch (MongoDB\Driver\Exception\Exception $e) {
            echo $e->getMessage(), "\n";
        }

    }

    public function select($table, $rows = '*', $where = null, $order = null, $sort_direction = self::SORT_ASC, $limit = null, $offset = null)
    {
        $result = array();

        if ($sort_direction == self::SORT_DESC) {
            $sort = '-1';
        } else {
            $sort = '1';
        }
        $filter = $where == null ? array() : $where;
        $order = $order == null ? '_id' : $order;
        $options = $sort_direction == null ? [] : [
            'sort' => [
                $order => $sort,
            ]
        ];
        if (is_array($rows)) {
          $columns = array();
          foreach ($rows as $row) {
            $columns[$row] = 1;
          }
          $columns['_id'] = 0;
          $options['projection'] = $columns;
        }
        if ($offset != null) {
            array_push($options, 'skip', $offset);
        }
        if ($limit != null) {
            array_push($options, 'limit', $limit);
        }
        $query = new MongoDB\Driver\Query($filter, $options);
        try {
            $this->cursor = $this->conn->executeQuery($this->db_name.'.'.$table, $query);
            $this->cursor = $this->cursor->toArray();
            foreach ($this->cursor as $objct) {
                $arr = json_decode(json_encode($objct), true);
                array_push($result, array_splice($arr, 1));
            }
        } catch (MongoDB\Driver\Exception\Exception $e) {
            echo $e->getMessage(), "\n";
        }
        return $result;
    }

    public function insert($table, $values, $rows = null)
    {
        $bulk = new MongoDB\Driver\BulkWrite;
        $id = $bulk->insert($values);
        try {
            $result = $this->conn->executeBulkWrite("{$this->db_name}.{$table}", $bulk);
        } catch (MongoDB\Driver\Exception\Exception $e) {
            echo $e->getMessage(), "\n";
        }
    }

    public function delete($table, $where, $multi = false)
    {
        $filter = $where;
        $options = !$multi ? ["limit" => 1] : [];
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->delete($filter, $options);
        try {
            $result = $this->conn->executeBulkWrite("{$this->db_name}.{$table}", $bulk);
        } catch (MongoDB\Driver\Exception\Exception $e) {
            echo $e->getMessage(), "\n";
        }
    }

    public function update($table, $rows, $where, $condition = "=")
    {
        $options = ["limit" => 1];
        $bulk = new MongoDB\Driver\BulkWrite;
        foreach ($rows as $key => $value) {
          $update = ['$set' => array($key => $value)];
          $bulk->update($where, $update, $options);
        }
        try {
            $result = $this->conn->executeBulkWrite("{$this->db_name}.{$table}", $bulk);
            return true;
        } catch (MongoDB\Driver\Exception\Exception $e) {
            echo $e->getMessage(), "\n";
        }
    }
}
