<?php
class BaseDatabase {

    protected $db_type;
    protected $db_user;
    protected $db_pass;
    protected $db_address;
    protected $db_name;
    protected $charset;
    public $myconn;

    public function __construct($DB_TYPE, $DB_ADDRESS, $DB_USER, $DB_PASS,
                                $DB_NAME, $CHARSET = 'utf8')
    {
        $this->db_type = $DB_TYPE;
        $this->db_address = $DB_ADDRESS;
        $this->db_user = $DB_USER;
        $this->db_pass = $DB_PASS;
        $this->db_name = $DB_NAME;
        $this->charset = $CHARSET;
    }

    public function connect()
    {

        if ($this->db_type == 'mongodb') {
            $this->myconn = new MongoDB\Driver\Manager("mongodb://{$this->db_user}:{$this->db_pass}@{$this->db_address}:27017/{$this->db_name}");
        }
        else {
            $dsn = "{$this->db_type}:host={$this->db_address};dbname={$this->db_name}".
                 ";charset={$this->charset}";
            $opt = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            $this->myconn = new PDO($dsn, $this->db_user, $this->db_pass, $opt);
        }
    }
}
