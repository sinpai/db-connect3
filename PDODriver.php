<?php
require_once "DriverInterface.php";

class PDODriver implements DriverInterface
{

    private $db_name;
    private $myconn;

    public function __construct($connection, $db_name)
    {
        if ($connection) {
            $this->myconn = $connection;
            $this->db_name = $db_name;
        } else {
            echo "Не удалось подключится к БД";
        }
    }

    public function createTable($name, $options = NULL)
    {
        $createOptions = '';
        foreach ($options as $key => $value) {
            $createOptions.=$key.' '.$value;
            $createOptions.=', ';
        }
        $createOptions = substr($createOptions, 0, -2);
        $this->myconn->query("DROP TABLE IF EXISTS {$name}");
        $created = $this->myconn->query("CREATE TABLE {$name}({$createOptions})");
        if (!$created) {
            echo "Не удалось создать таблицу: (" . $this->myconn->errno . ") " .
                                                 $this->myconn->error;
            return false;
        } else {
            return true;
        }
    }

    public function alterTable($name, $options, $field)
    {
        $alter = "ALTER TABLE {$name} ".$options;
        $changed = $this->myconn->query($alter);
        if (!$changed) {
            echo "Не удалось изменить таблицу: (" . $this->myconn->errno . ") " .
                                                  $this->myconn->error;
            return false;
        } else {
            return true;
        }
    }

    public function select($table, $rows = '*', $where = null, $order = null, $sort_direction = null, $limit = null, $offset = null)
    {
        $query_array = array();
        $select_result = array();

        $query_string = 'SELECT '.$rows.' FROM '.$table;
        if ($where != null) {
            if (is_array($where)) {
                $where_str = ' WHERE ';
                $where_headers = array_keys($where);
                for( $where_count = 0; $where_count < count($where); $where_count++) {
                    $where_str.="{$where_headers[$where_count]} = :{$where_headers[$where_count]}";
                    $where_str.=" AND ";
                }
                $query_string.= substr($where_str, 0, -5);
                foreach ($where as $wk => $wv) {
                    $query_array["{$wk}"] = $wv;
                }
            }
        }
        if ($order != null) {
            $query_string .= ' ORDER BY :order';
            $query_array['order'] = $order;
        }
        if ($sort_direction != null) {
            $query_string .= ' :sort_direction ';
            $query_array['sort_direction'] = $sort_direction;
        }
        if ($limit != null) {
            $query_string .= ' LIMIT :limit';
            $query_array['limit'] = $limit;
        }
        if ($offset != null) {
            $query_string .= ' OFFSET :offset';
            $query_array['offset'] = $offset;
        }

        $query = $this->myconn->prepare($query_string);
        if ($this->tableExists($table)) {
            if ($query->execute($query_array)) {
                $query_res = $query->fetchAll(PDO::FETCH_ASSOC);
                $this->numResults = count($query_res);
                for ($results_count = 0; $results_count < $this->numResults; $results_count++) {
                    $results_array = $query_res[$results_count];
                    $key = array_keys($results_array);
                    for ($count_row = 0; $count_row < count($key); $count_row++) {
                        if (!is_int($key[$count_row])) {
                            if ($query->rowCount() > 1) {
                                $select_result[$results_count][$key[$count_row]] = $results_array[$key[$count_row]];
                            } else if($query->rowCount() < 1) {
                                $select_result = null;
                            } else {
                                $select_result[$key[$count_row]] = $results_array[$key[$count_row]];
                            }
                        }
                    }
                }
                return $select_result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function insert($table, $values, $rows = null)
    {
        if ($this->tableExists($table)) {
            $insert = 'INSERT INTO '.$table;
            if ($rows != null) {
                $insert .= ' ( :rows )';
                $query_array['rows'] = $rows;
            }
            $query_headers = array_keys($values);

            $insert .= ' VALUES ( '.implode(',', array_map(function($header) { return ':'.$header.' '; }, $query_headers)).')';
            $ins = $this->myconn->prepare($insert);
            if ($ins->execute($values)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function delete($table, $where = null, $multi)
    {
        if ($this->tableExists($table)) {
            if($where == null){
                $delete = 'DELETE '.$table;
            } else {
                $delete = 'DELETE FROM '.$table.' WHERE '.$where;
            }
            if ($this->myconn->query($delete)) {
                return true;
            } else {
               return false;
            }
        } else {
            return false;
        }
    }

    public function update($table, $rows, $where, $condition)
    {
        $query_array = array();
        $values_str = '';

        $update = 'UPDATE '.$table.' SET ';
        if ($this->tableExists($table)) {
            if ($where != null) {
                if (is_array($where)) {
                    $where_str = ' WHERE ';
                    $where_headers = array_keys($where);
                    for( $where_count = 0; $where_count < count($where); $where_count++) {
                        $where_str.="{$where_headers[$where_count]} = :{$where_headers[$where_count]}";
                        $where_str.=" AND ";
                    }
                    $where_str = substr($where_str, 0, -5);
                    foreach ($where as $where_key => $where_value) {
                        $query_array["{$where_key}"] = $where_value;
                    }
                }
            }

            $keys = array_keys($rows);
            for ($rows_count = 0; $rows_count < count($rows); $rows_count++) {
                $values_str.="{$keys[$rows_count]} = :{$keys[$rows_count]}, ";
                $query_array["{$keys[$rows_count]}"] = $rows[$keys[$rows_count]];
            }

            $values_str = substr($values_str, 0, -2);

            $update.=$values_str.$where_str;
            $update_result = $this->myconn->prepare($update);

            if ($update_result->execute($query_array)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function tableExists($table)
    {
        $tablesInDb = $this->myconn->query('SHOW TABLES FROM '.$this->db_name.
                                           ' LIKE "'.$table.'"');
        if($tablesInDb){
            if ($tablesInDb->rowCount()==1) {
                return true;
            } else {
                return false;
            }
        }
    }
}
