<?php
require_once 'MongoDBDriver.php';
require_once 'PDODriver.php';

class DbManager
{
    private $conn;
    private $db_name;
    protected $driverStore = array();

    public function __construct($connection, $db_name, $db_type)
    {
        $connection->connect();
        $this->conn = $connection->myconn;
        $this->db_name = $db_name;
        $this->db_type = $db_type;
        $this->driver = $this->getDriver();
    }

    public function createTable($name, $options = null)
    {
        $this->driver->createTable($name, $options);
    }

    public function alterTable($name, $options, $field)
    {
        $this->driver->alterTable($name, $options, $field);
    }

    public function select($table, $rows = '*', $where = null, $order = null, $sort_direction = null, $limit = null, $offset = null)
    {
        return $this->driver->select($table, $rows, $where, $order, $sort_direction, $limit, $offset);
    }

    public function insert($table, $values, $rows = null)
    {
        $this->driver->insert($table, $values, $rows);
    }

    public function delete($table, $where, $multi = false)
    {
        $this->driver->delete($table, $where, $multi);
    }

    public function update($table, $rows, $where, $condition = "=")
    {
        $this->driver->update($table, $rows, $where, $condition);
    }

    private function getDriver() {
        if (!array_key_exists($this->db_type, $this->driverStore)) {
            $this->driverStore[$this->db_type] = $this->makeNewDriver();
        }
        return $this->driverStore[$this->db_type];
    }

    private function makeNewDriver() {
        if (is_a($this->conn, 'MongoDB\Driver\Manager')) {
            return new MongoDBDriver($this->conn, $this->db_name);
        } else {
            return new PDODriver($this->conn, $this->db_name);
        }
    }
}
