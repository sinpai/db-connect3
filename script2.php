<?php

require_once "BaseDatabase.php";
require_once "DbManager.php";
require_once "ResultsFormatter.php";

include 'variables.php';
include 'helper.php';

$table = 'test';
$new_field = 'surname';
$query_result = array();

$db = new BaseDatabase($DB_TYPE, $DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME);
$dbManager = new DbManager($db, $DB_NAME, $DB_TYPE);

$db_options = array("id" => "INT", "Username" => "nvarchar(20)", "Description" => "nvarchar(100)");
$db_options_keys = array_keys($db_options);
$dbManager->createTable($table, $db_options);

// Добавляю 100 строк
$rows_number = 100;
for ($i=0; $i <= $rows_number; $i++){
    $dbManager->insert($table,array(
        $db_options_keys[0] => $i,
        $db_options_keys[1] => "Name".$i,
        $db_options_keys[2] => "Description".(time()+$i)
    ));
}

// Добавляю колонку после имени
$dbManager->alterTable($table, "ADD {$new_field} nvarchar(50) AFTER Username", array($new_field => null));

// Меняю рандомное количество строк на рандомные фамилии
$rand_number = rand(1, $rows_number);
$rand_ids = array();
for ( $i=0; $i <= $rand_number; $i++) {
    array_push($rand_ids, rand(1, $rand_number));
}

array_push($db_options_keys, $new_field);
foreach ($rand_ids as $rand_ids_value){
    $dbManager->update($table,array($new_field => generateRandomString(10)),array('id' => $rand_ids_value),"=");
}

// Вывожу результаты
$query_result = $dbManager->select($table);

// как CSV
$db_options_keys = array_keys($query_result[0]);
$rf = new ResultsFormatter();
$csv_output = fopen("outputs/output.csv",'w');
foreach($rf->formatAsCSV($query_result, $db_options_keys) as $csv_line) {
  fputcsv($csv_output, $csv_line);
}
fclose($csv_output);

// как HTML слабый
$html_output = fopen("outputs/output.html", 'w');
fwrite($html_output, $rf->formatAsHTML($query_result, $db_options_keys, $rf->HTML_OUTPUT_MIN));
fclose($html_output);

// как HTML с заголовком
$html_output2 = fopen("outputs/output2.html", 'w');
fwrite($html_output2, $rf->formatAsHTML($query_result, $db_options_keys, $rf->HTML_OUTPUT_DEFAULT));
fclose($html_output2);

// как HTML с заголовком и хедером
$html_output3 = fopen("outputs/output3.html", 'w');
fwrite($html_output3, $rf->formatAsHTML($query_result, $db_options_keys, $rf->HTML_OUTPUT_FULL));
fclose($html_output3);

// в STDOUT (консоль)
$rf->formatToStdout($query_result, $db_options_keys);

// в файл
$txt_output = fopen("outputs/output.txt", 'w');
fwrite($txt_output, $rf->formatToStdout($query_result, $db_options_keys, 'file'));
fclose($txt_output);
