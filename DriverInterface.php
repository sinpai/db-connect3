<?php

interface DriverInterface
{
    public function createTable($name, $options);
    public function alterTable($name, $options, $field);
    public function select($table, $rows, $where, $order, $sort_direction, $limit, $offset);
    public function insert($table, $values, $rows);
    public function delete($table, $where, $multi);
    public function update($table, $rows, $where, $condition);
}
