<?php

require_once "BaseDatabase.php";
require_once "DbManager.php";
require_once "ResultsFormatter.php";
require_once "CSVManager.php";

include 'variables.php';
include 'helper.php';

$table = 'test';

$db = new BaseDatabase($DB_TYPE, $DB_ADDRESS, $DB_USER, $DB_PASS, $DB_NAME);
$dbManager = new DbManager($db, $DB_NAME, $DB_TYPE);

try {
    $csv = new CSVManager("inputs/input.csv");
    $getCsv = $csv->getCSV();
    $headers = array_shift($getCsv);
    foreach ($getCsv as $value) {
        $queryArray = array(
            $headers[0] => $value[0],
            $headers[1] => $value[1],
            $headers[2] => $value[2],
            $headers[3] => $value[3]
        );
        if ($dbManager->select( $table,
                                "*",
                                array('id' => $value[0]) )) {
            $dbManager->update( $table,
                                array_slice($queryArray, 1),
                                array('id' => $value[0]), "=" );
            print_r($value[0]);
            echo 'Update!';
        } else {
            try {
                $dbManager->insert($table, $queryArray);
            } catch (Exception $e) {
                echo "Ошибка вставки данных" . $e->getMessage();
            }
        }
        if ($value[0] % 99 == 0) {
          echo ".";
        }
    }
}
catch (Exception $e) {
    echo "Ошибка: " . $e->getMessage();
}
